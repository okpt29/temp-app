# Über die App
In meiner App habe ich 3 APIs ( Meteo , GeoDB und REST Countries ) benötigt. Im Folgenden möchte ich zuerst den Dienst der App erläutern.
Meine App heißt Temp-App, mit dieser App kann ein Benutzer durch einen Stadtnamen die Temperatur sowie zusätzliche Informationen über die eingegebene Stadt erhalten. Der Ablauf meiner App ist wie folgt:  Zuerst nehme ich von Meteo Daten, wie die Temperatur und die Koordinaten / Position, und leite die Koordinaten in die zweite API (GeoDB) weiter um eine CityId zu bekommen, die dann wieder mit GeoDB aufgerufen wird um weitere Informationen über die Stadt zu bekommen. Zum Schluss werden diese Daten dann genutzt um weitere Informationen aus der REST Countries API zu holen. (Die Daten dafür werden jedoch nur zum Start einmal initial geladen). Aus dieser nehme ich dann zusätzliche Informationen wie Hauptstadt und Amtssprachen.
Ausnahmen: Meistens findet eine API eine Stadt nicht und kann deshalb meistens nur wenige oder gar keine Daten zurückliefern. Dies liegt daran, dass alle APIs aufeinander aufbauen.

# Installation

Man muss zuerst sicherstellen, dass NPM & Node installiert sind, dazu geht man in die Konsole und gibt **npm --version** sowie **node --version** ein. Sollte bei einem nichts zurück kommen oder eine Fehlermeldung auftreten, dann sollte man im Internet sich die neuste Version herunterladen und installieren.

Dann kann man den Code zu diesem Projekt aus GitLab herunterladen mit **git clone git@git.thm.de:okpt29/temp-app.git** über SSH oder mit **git clone https://git.thm.de/okpt29/temp-app.git** über HTTPS.
Anschließend navigiert man in den Ordner mit einer Konsole und gibt **npm start** im Ordner ein.
Die App sollte dann über *http://localhost:8080* erreichbar sein.