let express  = require('express');

let app = express()
let bodyparser = require('body-parser');
let session = require('express-session')
const request = require('request');
require('dotenv').config();
const port=process.env.PORT || 8080;

const meteoKey = process.env.API_METEO_KEY;
const geoKey = process.env.API_GEO_KEY;
if(meteoKey === undefined || meteoKey === ''){
  throw new Error("Missing Meteo Api-Key!");
} else if(geoKey === undefined || geoKey === ''){
  throw new Error("Missing GEO Api-Key!");
}

const iso2Data = JSON.parse(require('fs').readFileSync('./ISO2.json').toString());

function callMeteo(city, callback){
  request.get({
    url: 'https://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=' + meteoKey,
    json: true
  }, (error, response, body) => {
    if(error){
      console.error(error);
      callback({'error' : 'failed to call meteo!'});
    } else {
      callback(body)
    }
  });
}

function callGEOInfo(id, callback){
  request.get({
    method: 'GET',
    url: 'https://wft-geo-db.p.rapidapi.com/v1/geo/cities/' + id,
    json: true,
    headers: {
      'x-rapidapi-host': 'wft-geo-db.p.rapidapi.com',
      'x-rapidapi-key': geoKey,
      useQueryString: true
    }
  }, (error, response, body) => {
    if(error){
      console.error(error);
      callback({'error' : 'failed to call geo info!'});
    } else {
      callback(body);
    }
  });
}

let countries = []
callRestCountries((body) => countries = body);

function callRestCountries(callback){
  request({
    method: 'GET',
    url: 'https://ajayakv-rest-countries-v1.p.rapidapi.com/rest/v1/all',
    json: true,
    headers: {
      'x-rapidapi-host': 'ajayakv-rest-countries-v1.p.rapidapi.com',
      'x-rapidapi-key': geoKey,
      useQueryString: true
    }
  }, function (error, response, body) {
    if (error) throw new Error(error);

    callback(body);
  });
}

function callGEO(coords, cityName, callback){
  request.get({
    method: 'GET',
    url: 'https://wft-geo-db.p.rapidapi.com/v1/geo/cities',
    qs: {
        limit: '1',
        includeDeleted: 'ALL', 
        location: (coords[0] >= 0 ? '+' + coords[0] : coords[0]) +
                  (coords[1] >= 0 ? '+' + coords[1] : coords[1]),
        radius: '10',
        namePrefix: cityName,
        types: 'CITY'
      },
    json: true,
    headers: {
      'x-rapidapi-host': 'wft-geo-db.p.rapidapi.com',
      'x-rapidapi-key': geoKey,
      useQueryString: true
    }
  }, (error, response, body) => {
    if(error){
      console.error(error);
      callback({'error' : 'failed to call geo!'});
    } else {
      if(body !== undefined && body.data !== undefined && body.data[0] !== undefined){
        setTimeout(() => {
          callGEOInfo(body.data[0].id, callback);
        }, 1100);
      } else {
        callback({'error': 'GEO DB didnt contained city ' + cityName + '!'});
      }
    }
  });
}

// template Motor
app.set('view engine','ejs')
app.set('views', __dirname + '/../views/page')

// Middleware
app.use(express.static('../public'));
app.use(bodyparser.urlencoded({ extended: false}))
app.use(bodyparser.json())
app.use(session({
  secret: 'Gelberblitz',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: true }
}));

app.use((err, req, res, next) => {
  console.error(err.stack);
  res.type('text/plain');
  res.status(500);
  res.send('500 - Internal server error');
});

app.get('/', (req, res) =>{
  res.render('index.ejs', {
    text: "",
    city: "",
    country: "",
    population: "",
    capital: "" ,
    text_error: ""
  });
});

app.post('/action', (request, response)=> {
  if(request.body.message === undefined || request.body.message === ''){
    response.render('index.ejs', {
      text: '',
      text_error: 'Sie haben keine Stadt angegeben!',
      city: ''
    })
  } else {
    callMeteo(request.body.message, (data) => {
      if(data.error){
        response.render('index.ejs', {
          text: '',
          text_error: data.error
        })
        return;
      }else if(data.cod === '404'){
        response.render('index.ejs', {
          text: '',
          text_error: 'Stadt nicht gefunden!'
        })
        return;
      }
      callGEO([data.coord.lat, data.coord.lon], request.body.message, (geoData) => {
        const temp = Math.floor((data.main.temp - 273.15) * 100) / 100;
        if(geoData.error){
          response.render('index.ejs', {
            text: temp,
            city: request.body.message,
            text_error :'Zusätzliche Daten zur Stadt ' + request.body.message + ' wurden nicht gefunden!'
          })
          return
        }
        const city = geoData.data.name
        let capital = 'Unbekannt'
        let languages = [];
        for(let i in countries){
          if(geoData.data.country.indexOf(countries[i].name) > -1){
            capital = countries[i].capital;
            for(let j in countries[i].languages){
              const key = countries[i].languages[j]
              languages.push(iso2Data[key] || key)
            }
            break;
          }
        }
        const country = geoData.data.country
        const population = new Intl.NumberFormat('de-De').format(geoData.data.population)
        response.render('index.ejs', {
          text: temp,
          city: '\n' + city,
          country: '\n' + country,
          population:'\n' + population,
          capital: '\n' + capital,
          languages: languages,
          text_error: ''
        });
      });
    });
  }
})

/*let server = http.createServer()

server.on('request', (request, Response)=>{
  fs.readFile('index.html', (err,data)=>{
    if(err) {
        Response.writeHead(404)
        Response.end('Diese Datei existiert leider nicht')
    }else{
        Response.writeHead(200,{
            'content-type':'text/html; charset=utf-8'
        })
        Response.end(data)
    }
    
  })
})*/
app.listen(port, () => {
  console.log("Express started on http://localhost:8080 !");
});
